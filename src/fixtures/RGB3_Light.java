package fixtures;

import java.awt.Color;
import java.awt.Graphics;

public class RGB3_Light {

	int x, y, address;

	public RGB3_Light(int x, int y, int address){
		this.x = x;
		this.y = y;
		this.address = address;
	}

	public void draw(int[] patch, Graphics g){
		g.setColor(new Color(patch[address], patch[address + 1], patch[address + 2]));
		g.fillRect(x, y, 25, 25);
		g.setColor(Color.WHITE);
		g.drawRect(x, y, 25, 25);
	}
}