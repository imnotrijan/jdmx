package main;

import java.lang.Runnable;
import java.lang.Thread;
import java.awt.Canvas;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.image.BufferStrategy;
import java.awt.image.BufferedImage;
import java.awt.image.DataBufferInt;
import javax.swing.JFrame;
import java.util.List;
import java.util.ArrayList;

import fixtures.RGB3_Light;

public class Test extends Canvas implements Runnable {

	final int WIDTH = 500;
	final int HEIGHT = 500;

	List<RGB3_Light> lights = new ArrayList<RGB3_Light>();
	int[] patch = new int[256];

	BufferedImage image = new BufferedImage(WIDTH, HEIGHT, BufferedImage.TYPE_INT_RGB);
	private int[] pixels = ((DataBufferInt) image.getRaster().getDataBuffer()).getData();

	boolean running = false;
	Thread thread;
	JFrame frame;

	String scene =
		"0:0,1:0,2:0\n" +		
		"0:255,2:255\n" +
		"\n" +
		"\n" +
		"\n" +
		"\n" +
		"\n" +
		"\n" +
		"\n" +
		"\n" +
		"\n" +
		"\n" +
		"\n" +
		"\n" +
		"\n" +
		"\n" +
		"\n" +
		"\n" +
		"\n" +
		"\n" +
		"\n" +
		"\n" +
		"\n" +
		"\n" +
		"\n" +
		"\n" +
		"\n" +
		"\n" +
		"\n" +
		"\n" +
		"\n" +
		"\n" +
		"\n" +
		"\n" +
		"\n" +
		"\n" +
		"\n" +
		"\n" +
		"1:255\n";
	int scene_index = 0;

	public Test(){
		setPreferredSize(new Dimension(WIDTH, HEIGHT));
		frame = new JFrame("Display");
		frame.setResizable(false);
		frame.add(this);
		frame.pack();
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setLocationRelativeTo(null);
		frame.setVisible(true);
		lights.add(new RGB3_Light(100, 100, 0));
		lights.add(new RGB3_Light(200, 300, 1));
		start();
		//stop();
	}

	public static void main(String[] args) {
		System.out.println("Kaizo!");
		Test t = new Test();
	}

	public synchronized void start(){
		running = true;
		thread = new Thread(this);
		System.out.println("Starting thread...!");
		thread.start();
		patch[0] = 255;
	}

	public void run(){
		System.out.println("Thread started!");
		long lastTime = System.currentTimeMillis();
		long currentTime = lastTime;
		while (running){
			while (currentTime - lastTime < 100){
				currentTime = System.currentTimeMillis();
			}
			System.out.println("Kaizo!");
			lastTime = currentTime;
			update();
			render();
		}
		System.out.println("Thread ended!");
	}

	void performPatch(){
		String[] lines = scene.split("\n");
		if (scene_index == lines.length){
			scene_index = 0;
		}
		String line = lines[scene_index];
		if (!line.equals("") && !line.startsWith("#")){
			String[] pairs = line.split(",");
			System.out.print("Patch: ");
			for (String pair : pairs){
				String[] pair_arr = pair.split(":");
				System.out.print(pair_arr[0] + ":" + pair_arr[1]);
				patch[Integer.parseInt(pair_arr[0])] = Integer.parseInt(pair_arr[1]);
			}
			System.out.println();
		}
		scene_index++;
	}

	public void update(){
		performPatch();
	}

	public void render(){
		BufferStrategy bs = getBufferStrategy();
		if (bs == null){
			createBufferStrategy(2);
			return;
		}

		for (int i = 0; i < pixels.length; i++){
			pixels[i] = 0x000000;
		}

		Graphics g = bs.getDrawGraphics();
		g.setColor(Color.BLACK);
		g.drawImage(image, 0, 0, WIDTH, HEIGHT, null);
		for (RGB3_Light l : lights){
			l.draw(patch, g);
		}
		g.dispose();
		bs.show();
	}

	public synchronized void stop(){
		running = false;
		System.out.println("Stopping thread...!");
		try {
			thread.join();
		} catch (InterruptedException e){
			e.printStackTrace();
		}
		System.out.println("Stopped thread!");
	}

}