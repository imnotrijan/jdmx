import os, subprocess

sources = ""

for dirName, _, fileList in os.walk("src"):
	for fname in fileList:
		sources += dirName + "/" + fname + " "

subprocess.call("javac -d bin -sourcepath src " + sources, shell=True)