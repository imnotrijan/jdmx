#!/bin/bash
if [ $# -lt 1 ]
then
	echo "Provide entry point (main class)"
else
	java -cp bin $1
fi